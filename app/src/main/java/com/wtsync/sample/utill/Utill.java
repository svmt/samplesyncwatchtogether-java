package com.wtsync.sample.utill;

import android.content.Context;
import android.widget.Toast;

import com.wtsync.sample.R;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Utill {

    public static void showError(final Context context, final String errorMessage) {
        showToast(context, errorMessage);
    }

    public static void showToast(final Context context, final String errorMessage) {
        final Toast t = Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT);
        t.show();
    }
}
