package com.wtsync.sample.ui;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.wtsync.sample.R;
import com.wtsync.sample.ui.ClientsAdapter.ClientViewHolder;
import com.wtsync.sdk.data.SyncClient;
import java.util.ArrayList;
import java.util.List;

public class ClientsAdapter extends RecyclerView.Adapter<ClientViewHolder> {

    private final List<SyncClient> mClientsList = new ArrayList<>();

    @NonNull
    @Override
    public ClientViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        return new ClientViewHolder(LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.item_clients, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ClientViewHolder viewHolder, final int position) {
        final SyncClient syncClient = mClientsList.get(position);
        viewHolder.name.setText(syncClient.getName());
        viewHolder.local.setText(viewHolder.itemView.getContext().getString(syncClient.isLocal() ? R.string.txt_local : R.string.txt_remote));
        viewHolder.leader.setText(viewHolder.itemView.getContext().getString(syncClient.isLeader() ? R.string.txt_leader : R.string.txt_empty_string));
    }

    @Override
    public int getItemCount() {
        return mClientsList.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    void addClientsList(final List<SyncClient> viewerList) {
        mClientsList.addAll(viewerList);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    void clearClients() {
        mClientsList.clear();
        notifyDataSetChanged();
    }

    String getClientName(String id) {
        for (SyncClient client : mClientsList) {
            if (id.equals(client.getName())) {
                return client.getName();
            }
        }
        return "";
    }

    static class ClientViewHolder extends RecyclerView.ViewHolder {
        final AppCompatTextView name, local, leader;

        ClientViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
            local = v.findViewById(R.id.local);
            leader = v.findViewById(R.id.leader);
        }
    }
}