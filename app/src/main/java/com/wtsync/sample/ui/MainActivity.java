package com.wtsync.sample.ui;

import static java.lang.String.format;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.wtsync.sample.R;
import com.wtsync.sample.utill.Utill;
import com.wtsync.sdk.SyncListener;
import com.wtsync.sdk.SyncSdk;
import com.wtsync.sdk.data.SyncClient;
import com.wtsync.sdk.data.SyncInfo;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements SyncListener, Player.Listener,
    MediaSourceEventListener {

    // Const string keys to store values
    private final String SYNC_SERVICE_KEY = "sync_service_key";
    // Bundle object to save and get values
    private final Bundle mBundle = new Bundle();

    private SyncSdk mSyncSdk = null;
    private String mDisplayName, mVideoUrl;
    private String mAccessToken = "PUT_TOKEN";
    private long mCalculatedCurrentPosition = 0L;
    private Timer mTimer;
    private ExoPlayer mExoPlayer;
    private StyledPlayerView mExoPlayerView = null;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private ClientsAdapter mClientsAdapter = new ClientsAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // To prevent screen to be switched off
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Extract session's params
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                mDisplayName = String.valueOf(getIntent().getExtras().get(StartActivity.GROUP_NAME_CODE));
                mVideoUrl = String.valueOf(getIntent().getExtras().get(StartActivity.VIDEO_URL_CODE));
            }
        }
        mExoPlayer = new ExoPlayer.Builder(this).build();

        RecyclerView recyclerViewers = (RecyclerView) findViewById(R.id.recycler_viewers);
        recyclerViewers.setAdapter(mClientsAdapter);

        final AppCompatButton btnStartSync = findViewById(R.id.btn_start_sync);
        final AppCompatButton btnStopSync = findViewById(R.id.btn_stop_sync);
        final AppCompatButton btnGroupPlay = findViewById(R.id.btn_group_play);
        final AppCompatButton btnGroupPause = findViewById(R.id.btn_group_pause);
        final AppCompatButton btnGroupPrevious = findViewById(R.id.btn_group_seek_previous);
        final AppCompatButton btnGroupNext = findViewById(R.id.btn_group_seek_next);

        btnStartSync.setOnClickListener(v -> {
            if (mSyncSdk != null) {
                mSyncSdk.startSync();
                btnStartSync.setEnabled(false);
                btnStopSync.setEnabled(true);
                btnGroupPlay.setEnabled(true);
            }
        });
        btnStopSync.setOnClickListener(v -> {
            if (mSyncSdk != null) {
                mSyncSdk.stopSync();
                btnStartSync.setEnabled(true);
                btnStopSync.setEnabled(false);
                mClientsAdapter.clearClients();
            }
        });
        btnGroupPlay.setOnClickListener(v -> {
            if (mExoPlayer != null && mSyncSdk != null) {
                mSyncSdk.groupPlay();
                mExoPlayer.play();
            }
        });
        btnGroupPause.setOnClickListener(v -> {
            if (mExoPlayer != null && mSyncSdk != null) {
                mSyncSdk.groupPause();
                mExoPlayer.pause();
            }
        });

        btnGroupPrevious.setOnClickListener(v -> {
            seekToPosition(false, getPlayerCalculatedPosition());
        });

        btnGroupNext.setOnClickListener(v -> {
            seekToPosition(true, getPlayerCalculatedPosition());
        });

        initPlayer();
        initSyncSdk();
    }

    private void initSyncSdk() {
        mSyncSdk = new SyncSdk.SyncSdkBuilder()
            .accessToken(mAccessToken)
            .name(mDisplayName)
            .syncListener(this)
            .build(this);
    }

    private void initPlayer() {
        CookieManager defaultCookieManager = new CookieManager();
        defaultCookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);

        mExoPlayerView = findViewById(R.id.player);
        mExoPlayerView.requestFocus();

        mExoPlayer.addListener(this);
        mExoPlayer.setPlayWhenReady(true);
        mExoPlayerView.setPlayer(mExoPlayer);

        onPlay(mVideoUrl);
    }

    private void seekToPosition(final boolean isForward, final long position) {
        if (mExoPlayer.isPlaying() && mExoPlayer.getPlaybackState() == Player.STATE_READY
                && !mExoPlayer.isCurrentMediaItemLive()) {
            final int sign = isForward ? 1 : -1;
            final long seekTo = position + (300_000L * sign);  // seek to 5 minutes forward or back
            if (seekTo >= 0) {
                if (mExoPlayer != null && mSyncSdk != null) {
                    mSyncSdk.groupSeek(seekTo);
                    mExoPlayer.seekTo(seekTo);
                    Utill.showToast(getApplicationContext(),
                            "onSeekGroupEvent sent position=" + dateFormat.format(new Date(seekTo)) + ")");
                }
            }
        }
    }

    private void onPlay(String url) {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        mTimer = new Timer(String.valueOf(System.currentTimeMillis()));
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (mExoPlayer.isPlaying()) {
                        ((AppCompatTextView) findViewById(R.id.player_position))
                            .setText(dateFormat.format(new Date(onGetPlayerPosition())));
                    }
                });
            }
        }, 0, 300);
        try {
            mExoPlayer.setMediaSource(
                buildMediaSource(
                    Uri.parse(url),
                    new Handler(Looper.getMainLooper())
                )
            );
            mExoPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MediaSource buildMediaSource(Uri uri, Handler handler) {
        HlsMediaSource mediaSource = new HlsMediaSource.Factory(new DefaultDataSource.Factory(this))
            .setAllowChunklessPreparation(true)
            .createMediaSource(MediaItem.fromUri(uri));
        mediaSource.addEventListener(handler, this);
        return mediaSource;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSyncSdk != null) {
            mSyncSdk.stopSync();
        }
        if (mExoPlayer != null) {
            mExoPlayer.pause();
        }
        findViewById(R.id.btn_start_sync).setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if Sync service was stopped because of app was putted into background
        if (mBundle.getBoolean(SYNC_SERVICE_KEY, false)) {
            // Clear Sync service value in the default state
            mBundle.putBoolean(SYNC_SERVICE_KEY, false);
            // Start Sync service
            if (mSyncSdk != null) {
                mSyncSdk.startSync();
                (findViewById(R.id.btn_start_sync)).setEnabled(false);
            }
            // Resume ExoPlayer
            if (mExoPlayer != null) {
                mExoPlayer.play();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // If its onBackPressed, we do not suppose resuming Sync service automatically. so clear Sync service value in the default state
        mBundle.putBoolean(SYNC_SERVICE_KEY, false);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSyncSdk != null) {
            mSyncSdk.stopSync();
            mSyncSdk = null;
        }
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mExoPlayer != null) {
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer = null;
        }
        if (mExoPlayerView != null) {
            mExoPlayerView = null;
        }
    }

    @Override
    public void onPlaybackStateChanged(int state) {
        ProgressBar progress = findViewById(R.id.contentLoadingProgressBar);
        switch (state) {
            case Player.STATE_BUFFERING:
                progress.setVisibility(View.VISIBLE);
            case Player.STATE_READY:
                progress.setVisibility(View.GONE);
            case Player.STATE_ENDED:
                progress.setVisibility(View.GONE);
            case Player.STATE_IDLE:
                progress.setVisibility(View.GONE);
            default:
                progress.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPlayerError(PlaybackException error) {
        if (!isFinishing()) {
            final String errorMessage = "Error while playing stream";
            Utill.showError(this, errorMessage);
            if (mExoPlayer != null) {
                mExoPlayer.stop();
            }
            onPlay(mVideoUrl);
        }
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        String rate = String.valueOf(playbackParameters.speed);
        ((AppCompatTextView) findViewById(R.id.rate)).setText(rate);
    }

    @Override
    public void onClientList(List<SyncClient> clientList) {
        mClientsAdapter.clearClients();
        mClientsAdapter.addClientsList(clientList);
    }

    @Override
    public void onPlaybackFromPosition(long position) {
        if (mExoPlayer != null && mExoPlayer.isPlaying() && mExoPlayer.getPlaybackState() == Player.STATE_READY) {
            mExoPlayer.seekTo(position);
        }
    }

    @Override
    public void onSyncInfo(@NonNull final SyncInfo syncInfo) {
        long delta = syncInfo.getDelta();
        if (delta != 0) {
            ((AppCompatTextView) findViewById(R.id.delta)).setText(format("%s ms", delta));
        } else {
            ((AppCompatTextView) findViewById(R.id.delta)).setText(getString(R.string.txt_empty_string));
        }
        float accuracy = syncInfo.getAccuracy();
        if (accuracy > 0) {
            ((AppCompatTextView) findViewById(R.id.accuracy)).setText(String.format("%s %%", accuracy));
        } else {
            ((AppCompatTextView) findViewById(R.id.accuracy)).setText(getString(R.string.txt_empty_string));
        }
    }

    @Override
    public void onSyncDisconnected() {
        mClientsAdapter.clearClients();
        (findViewById(R.id.btn_start_sync)).setEnabled(true);
        (findViewById(R.id.btn_stop_sync)).setEnabled(false);
        Utill.showError(this, "Sync service was disconnected!");
    }

    @Override
    public void onSetPlaybackRate(float rate) {
        if (mExoPlayer != null) {
            mExoPlayer.setPlaybackParameters(new PlaybackParameters(rate));
        }
    }

    @Override
    public float onGetPlaybackRate() {
        if (mExoPlayer != null && mExoPlayer.isPlaying()) {
            if (mExoPlayer.getPlaybackParameters() != null) {
                return mExoPlayer.getPlaybackParameters().speed;
            }
        }
        return 0f;
    }

    @Override
    public long onGetPlayerPosition() {
        if (mExoPlayer != null && mExoPlayer.isPlaying()) {
            mCalculatedCurrentPosition = getPlayerCalculatedPosition();
            return mCalculatedCurrentPosition;
        }
        return 0L;
    }

    @Override
    public void onSyncGroupPlay(String clientId) {
        if (mExoPlayer != null) {
            mExoPlayer.play();
        }
        Utill.showToast(this, "From" + mClientsAdapter.getClientName(clientId) + "received onSyncGroupPlay");
    }

    @Override
    public void onSyncGroupPause(String clientId) {
        if (mExoPlayer != null) {
            mExoPlayer.pause();
        }
        Utill.showToast(this, "From" + mClientsAdapter.getClientName(clientId) + "received onSyncGroupPause");
    }

    @Override
    public void onSyncGroupSeek(String clientId, long position) {
        if (!mExoPlayer.isCurrentMediaItemLive()) {
            // if its VOD then player can seek to required position
            mExoPlayer.seekTo(position);
        }
        // Client should send success seek request every time onSyncGroupSeek called
        if (mSyncSdk != null) {
            mSyncSdk.groupSeekSuccess(position);
        }
        Utill.showToast(this, "From" + (clientId == null ? "Server" : mClientsAdapter.getClientName(clientId)) + "received onSyncGroupSeek");
    }

    private long getPlayerCalculatedPosition() {
        if (mExoPlayer != null) {
            long position = mExoPlayer.getCurrentPosition();
            long streamPosition = getPlayerPositionFromTimeLine(mExoPlayer.getCurrentTimeline());
            mCalculatedCurrentPosition = position + streamPosition;
            return mCalculatedCurrentPosition;
        }
        return 0;
    }

    private Long getPlayerPositionFromTimeLine(Timeline timeline) {
        long windowStartTimeMs = 0L;
        if (timeline == null || timeline.isEmpty()) {
            return windowStartTimeMs;
        }
        Timeline.Window window = timeline.getWindow(0, new Timeline.Window());
        if (window.windowStartTimeMs > windowStartTimeMs) {
            windowStartTimeMs = window.windowStartTimeMs;
        }
        return windowStartTimeMs;
    }
}