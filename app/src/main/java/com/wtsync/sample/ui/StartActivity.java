package com.wtsync.sample.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.wtsync.sample.BuildConfig;
import com.wtsync.sample.R;

public class StartActivity extends AppCompatActivity {

    public static final String GROUP_NAME_CODE = "group_name_code";
    public static final String VIDEO_URL_CODE = "video_url_code";

    private AppCompatEditText mDisplayName;
    private AppCompatEditText mStreamUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ((AppCompatTextView) findViewById(R.id.app_version)).setText(BuildConfig.VERSION_NAME);

        final SharedPreferences sharedPref =
                getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        mDisplayName = findViewById(R.id.display_name);
        mDisplayName.setText(sharedPref.getString(GROUP_NAME_CODE, ""));
        mStreamUrl = findViewById(R.id.stream_url);
        if (TextUtils.isEmpty(sharedPref.getString(VIDEO_URL_CODE, ""))) {
            mStreamUrl.setText(R.string.str_stream_url);
        } else {
            mStreamUrl.setText(sharedPref.getString(VIDEO_URL_CODE, ""));
        }
        // Click Listener for sign in button
        findViewById(R.id.sign_in).setOnClickListener(v -> signIn());
        // Done actions sign in button click
        mDisplayName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    signIn();
                    return true;
                }
                return false;
            }
        });
    }

    private void signIn() {
        final String displayName = String.valueOf(mDisplayName.getText());
        final String videoUrl = String.valueOf(mStreamUrl.getText());

        final SharedPreferences sharedPref =
                getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        sharedPref.edit().putString(GROUP_NAME_CODE, displayName).apply();

        startMainActivity(displayName, videoUrl);
    }

    private void startMainActivity(String displayName, String videoUrl) {
        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(GROUP_NAME_CODE, displayName);
        intent.putExtra(VIDEO_URL_CODE, videoUrl);
        startActivity(intent);
    }

}